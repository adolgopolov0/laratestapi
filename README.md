## команды запуска

`docker-compose exec db bash`

`docker-compose exec app bash`

`docker-compose exec webserver bash`


## Запуск

```bash
docker-compose up -d
```

```bash
docker-compose down
```

```bash
docker-compose exec app php artisan key:generate
```

## Адрес

http://0.0.0.0:8080/

## Миграции

```bash
docker-compose exec app php artisan migrate
```

## JWT
```bash
docker-compose exec app php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
```
```bash
docker-compose exec app php artisan jwt:secret
```


## Добавить пользователя 

```php
'name' => 'admin',
'email' => 'admin@admin.com',
'password' => Hash::make('admin123'),
```
`docker-compose exec app php artisan db:seed`

## получить токен при авторизации и подставлять его в Authorization: Bearer {{api-token}}

`{{host}}/api/auth/login`

`{{host}}/api/proxies/list`

`{{host}}/api/proxies/export`

## постман колекция запросов с вариантами 
[wiki/laravel-api.postman_collection.json](./wiki/laravel-api.postman_collection.json)
