<?php

namespace Tests\Unit\proxies;

use App\Services\Provider\DTO\ProxyDTO;
use App\Services\Provider\ProviderContract;
use Tests\TestCase;

class ServiceTest extends TestCase
{

    public function testLoadListProxies()
    {
        /** @var ProviderContract $provider */
        $provider = app()->make(ProviderContract::class);
        $collectionProxies = $provider->loadListProxies();
        $this->assertIsArray($collectionProxies);
        foreach ($collectionProxies as $eachProxy){
            $this->assertInstanceOf(ProxyDTO::class,$eachProxy);
            $this->assertNotEmpty($eachProxy->ip);
        }
    }
}
