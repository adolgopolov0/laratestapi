<?php

namespace Tests\Unit\proxies;

use App\Repositories\ProxyRepository;
use App\Services\Provider\DTO\ProxyDTO;
use App\Services\Provider\ProviderContract;
use Illuminate\Support\Collection;
use Tests\TestCase;

class RepositoryTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testGetProxies()
    {
        $proxy = new ProxyDTO();
        $proxy->ip = '0.0.0.0';
        $this->mock(ProviderContract::class)
            ->shouldReceive('loadListProxies')
            ->andReturn([$proxy]);
        /** @var ProxyRepository $repository */
        $repository = app()->make(\App\Repositories\ProxyRepository::class);
        $collection = $repository->getProxies();
        $this->assertInstanceOf(Collection::class,$collection);
        $this->assertInstanceOf(ProxyDTO::class,$collection->last());
    }
}
