<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExportRequest;
use App\Http\Resources\ProxiesResource;
use App\Repositories\ProxyRepository;
use App\Services\File\FileSendService;
use Illuminate\Http\Request;

class ProxyController extends Controller
{
    private FileSendService $fileSendService;
    private ProxyRepository $repository;

    public function __construct(
        FileSendService $fileSendService,
        ProxyRepository $repository
    )
    {
        $this->fileSendService = $fileSendService;
        $this->repository = $repository;
    }

    public function list(): ProxiesResource
    {
        return new ProxiesResource($this->repository->getProxies());
    }

    public function export(ExportRequest $request)
    {
        return  $this->fileSendService->sendFile(
            $this->repository->getProxies(),
            $request->get('format'),
            'proxies'
        );
    }
}
