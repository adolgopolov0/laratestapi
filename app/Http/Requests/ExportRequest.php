<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Http\Rules\ExportFormatRule;

final class ExportRequest extends \Illuminate\Foundation\Http\FormRequest
{
    public function rules()
    {
        return [
            'format' => ['required','string',new ExportFormatRule()]
        ];
    }
}
