<?php

declare(strict_types=1);

namespace App\Http\Resources;
use App\Services\Provider\DTO\ProxyDTO;

/**
 * @property ProxyDTO $resource
 */
final class ProxyResource extends \Illuminate\Http\Resources\Json\JsonResource
{
    public function toArray($request)
    {
        return [
            'ip' => $this->resource->ip,
            'port' => $this->resource->port,
            'login' => $this->resource->login,
            'password' => $this->resource->password,
        ];
    }
}
