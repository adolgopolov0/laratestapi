<?php

declare(strict_types=1);

namespace App\Http\Resources;
use App\Http\Resources\CommunityResource;
use App\Http\Resources\ProxyResource;
use App\Services\Provider\DTO\ProxyDTO;
use Illuminate\Http\Resources\Json\ResourceCollection;

final class ProxiesResource extends ResourceCollection
{
    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->collection = $this->collection->map(function (ProxyDTO $item){

            return new ProxyResource($item);
        });
    }

}
