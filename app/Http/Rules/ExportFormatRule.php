<?php

declare(strict_types=1);

namespace App\Http\Rules;

use App\Helpers\DictionaryHelper;
use Illuminate\Support\Str;

final class ExportFormatRule implements \Illuminate\Contracts\Validation\Rule
{
    /**
     * @inheritDoc
     */
    public function passes($attribute, $value)
    {
        return (bool) in_array($value, DictionaryHelper::getExportFormats());
    }

    /**
     * @inheritDoc
     */
    public function message()
    {
        return "Не верный формат для отображения данных";
    }
}
