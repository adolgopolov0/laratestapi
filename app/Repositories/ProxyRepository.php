<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Services\Provider\DTO\ProxyDTO;
use App\Services\Provider\ProviderContract;
use Illuminate\Support\Collection;

final class ProxyRepository
{
    private ProviderContract $provider;

    public function __construct(ProviderContract $provider)
    {
        $this->provider = $provider;

    }

    public function getProxies(): Collection
    {
        $collection = new Collection();
        foreach ($this->provider->loadListProxies() as $eachProxy) {
            $collection->add($eachProxy);
        }
        return $collection;
    }
}
