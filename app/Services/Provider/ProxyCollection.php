<?php

declare(strict_types=1);

namespace App\Services\Provider;
use App\Http\Resources\CommunityResource;
use App\Services\Provider\DTO\ProxyDTO;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

/** @method  ProxyDTO $resource) */
final class ProxyCollection extends Collection
{
    public function __construct()
    {

    }
}
