<?php

declare(strict_types=1);

namespace App\Services\Provider;

use Illuminate\Support\Collection;

interface ProviderContract
{
    public function connect(array $data): bool;

    public function loadListProxies(): array;
}
