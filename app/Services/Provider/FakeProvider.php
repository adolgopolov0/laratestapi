<?php

declare(strict_types=1);

namespace App\Services\Provider;

use App\Services\Provider\DTO\ProxyDTO;
use Faker\Generator;
use Illuminate\Support\Collection;

final class FakeProvider implements ProviderContract
{


    private Generator $faker;

    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }

    public function connect(array $data): bool
    {
        // TODO: Implement connect() method.
    }

    public function loadListProxies(): array
    {
        $list = [];
        for($i = rand(3,10);$i > 0; $i--){
            $dto = new ProxyDTO();
            $dto->ip = $this->faker->ipv4();
            $dto->port = (string) rand(80,2022);
            $dto->login = $this->faker->firstNameMale();
            $dto->password = $this->faker->password(8,10);
            $list[] = $dto;
        }
        return $list;
    }
}
