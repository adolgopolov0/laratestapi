<?php

declare(strict_types=1);

namespace App\Services\Provider\DTO;

final class ProxyDTO
{
    public string $ip;
    public string $port;
    public string $login;
    public string $password;
}
