<?php

namespace App\Services\File;

use App\Exceptions\StatisticException;
use App\Helper\ArrayHelper;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;
use \Illuminate\Http\Request;

class FileSendService
{

    public function sendFile(
        Collection $items,
        string $format,
        string $name = 'noname'): StreamedResponse
    {

        foreach ($items as $eachItem) {

        }

        $headers = [
            'Content-Type' => 'text/csv',
        ];
        $callback = function () use ($items, $format) {

            // Open output stream
            $handle = fopen('php://output', 'w');
            foreach ($items as $eachItem) {
                $proxyString = $format;
                $row = [];
                foreach ($eachItem as $key => $value) {
                    $proxyString = Str::replace($key,$value,$proxyString);
                }
                $row[] = $proxyString;
                fputcsv($handle, $row);
            }

            fclose($handle);
        };
        $headers['Content-Transfer-Encoding'] = 'binary';
        $headers['Pragma'] = 'public';
        $headers['Cache-Control'] = 'must-revalidate, post-check=0, pre-check=0, public';
        $date = Carbon::now()->format("Y_m_d_H_i_s");
        return response()->streamDownload($callback, "{$name}_{$date}.csv", $headers);
    }
}
