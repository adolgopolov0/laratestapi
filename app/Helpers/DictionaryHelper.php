<?php

declare(strict_types=1);

namespace App\Helpers;

final class DictionaryHelper
{
    const EXPORT_FORMAT = [
        'ip:port@login:password',
        'ip:port',
        'ip',
        'ip@login:password'
    ];

    public static function getExportFormats()
    {
        return self::EXPORT_FORMAT;
    }
}
